import javax.swing.*;
import java.awt.*;

public class Events {

	public static void main(String[] args) {
		
		JFrame frame = new JFrame("Ditt namn appen");
		JButton button = new JButton("Klicka mig");
		JLabel label = new JLabel("");
		JTextField textField = new JTextField(10);
		
		EventsClass e = new EventsClass(label, textField);
		
		frame.setSize(250, 200);
		frame.setDefaultCloseOperation(3);
		frame.setVisible(true);
		frame.setLocation(300, 200);
		button.setActionCommand("Knappen");
	
		button.addActionListener(e);
		
		frame.setLayout(new BorderLayout());
		
		JPanel ruta1 = new JPanel();
		JPanel ruta2 = new JPanel();
		
		ruta2.setLayout(new BorderLayout());
		
		ruta1.add(textField);
		ruta2.add(button);
		ruta1.add(label);
		
	
		frame.add(ruta1, BorderLayout.NORTH);
		frame.add(ruta2, BorderLayout.SOUTH);
		
	
	
	}

}
