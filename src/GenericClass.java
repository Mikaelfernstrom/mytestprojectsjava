/* Ett exempel p� generic metod
 * 
 * Generic anv�nds n�r man inte vet vilken datatyp som man ska f� in
 * Se metoden under hur man g�r
 *Vanliga metoder v�ljs f�rst om man har en overloaded metod
 */

public class GenericClass {

	public <T> void sam(T x){
		
		System.out.println(x);
		
	}
	
	public void sam(int x){
		System.out.println("Vanlig metod");
	}
}

/* Ett exempel p� generic class
 * 
 * public class GenericClass<T>{
 * 
 * public T metodNamn(T x){
 * 		return x
 * }}
 * 
 * Public Static void main(String[] args){
 * GenericClass<String> g = new GenericClass<String>();
 */
  
 
