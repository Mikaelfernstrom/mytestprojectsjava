
public class Arrays {

	public static void main(String[] args) {
		
		ArrayClass a = new ArrayClass("Anna", 1985);
		ArrayClass m = new ArrayClass("Micke", 1990);
		ArrayClass ma = new ArrayClass("Maja", 2012);
		
		//Tv� olika s�tt att deklarera sin array
		ArrayClass[] pp = new ArrayClass[5];
		//ArrayClass[] pp = {a,m,ma}; // Bra om man vet exakt hur m�nga v�rden man ska lagra
		
		pp[0] = a;
		pp[1] = m;
		pp[2] = ma;
		
		int x = pp[2].getYear();
		System.out.println(x);

	}

}
