import java.util.Calendar;
public class ArrayClass {

	private int year;
	public String name;
	
	public ArrayClass(String n, int a){
		name = n;
		year = a;
	}
	
	public int getYear(){
		Calendar c = Calendar.getInstance();
		int c_year = c.get(Calendar.YEAR);
		
		return c_year - year;
	}
}
