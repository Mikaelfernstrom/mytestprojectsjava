import java.util.Calendar;
public class ArrayListClass {

	private int year;
	public String name;
	
	public ArrayListClass(String n, int a){
		name = n;
		year = a;
	}
	
	public int getYear(){
		Calendar c = Calendar.getInstance();
		int c_year = c.get(Calendar.YEAR);
		
		return c_year - year;
	}
}