import java.awt.event.*;
import javax.swing.*;
public class EventsClass implements ActionListener {

	private JLabel l;
	private JTextField t;
	
	public EventsClass(JLabel label, JTextField text){
		
		l = label;
		t = text;
	}
	public void actionPerformed(ActionEvent e){
		 
		String message = t.getText();
		l.setText("Ditt namn �r " + message);
		
	}
	
}
