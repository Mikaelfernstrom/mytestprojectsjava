
public enum EnumClass {
	
	Kaffe(4),
	CocaCola(3),
	Fanta(3) {public String healthy(){return "Looks good!";}},
	RedBull(5) {public String healthy(){return "Red bull gives you wings!";}};

	private int power;
	
	EnumClass(int power){
		this.power = power;
	}
	
	public int getPower(){
		return power;
	}
	
	public String healthy(){
		return "Its not healthy but it is damn good!";
	}
}
