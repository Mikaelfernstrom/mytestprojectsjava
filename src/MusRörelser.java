import javax.swing.*;

public class MusRörelser {

	public static void main(String[] args) {
		
		JFrame frame = new JFrame();
		MusRörelserClass mus = new MusRörelserClass();
		
		frame.setSize(300, 300);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(3);
		
		frame.addMouseMotionListener(mus);
		
		frame.add(mus);

	}

}
