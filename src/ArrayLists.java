import java.util.ArrayList;

public class ArrayLists {

	public static void main(String[] args) {
		
		ArrayListClass a = new ArrayListClass("Anna", 1985);
		ArrayListClass b = new ArrayListClass("Micke", 1990);
		ArrayListClass c = new ArrayListClass("Maja", 2012);

		ArrayList<ArrayListClass> pp = new ArrayList<ArrayListClass>(); //generic arraylist
		
		pp.add(a);
		pp.add(b);
		pp.add(c);
		
		//pp.remove(b); // tar bort v�rdet b ur arrayen
		
		if(pp.contains(a)){
			System.out.println("Personen finns!");
		}
		else{
			System.out.println("Personen finns icke!");
		}
		
		int x = pp.size();
		System.out.println(x);
		
		ArrayListClass[] person = new ArrayListClass[x];
		pp.toArray(person);
		
		//Enhanced for loop
		for(ArrayListClass i : person){
			System.out.println(i.name);
		}
		
		
		
	}

}
