/**
 * 
 */

/**
 * @author Mikael
 *
 */
import javax.swing.*;
import java.awt.*;
public class jframe {

	public static void main(String[] args) {
		
		JFrame window = new JFrame("The windows name");
		
		window.setLocation(0, 0); // Where the window will appear on the screen (width, height)
		window.setResizable(false); // You can not adjust the size on the window if false
		
		window.setVisible(true); // Window will be visible
		window.setSize(300, 200); // Size on window
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Closes the whole process
		
		window.setTitle("The windows name"); // Here you can rename the title
		Container myContainer = window.getContentPane();
		
		
		myContainer.setLayout(new FlowLayout());
		myContainer.add(new JButton("Holla Bandola"));
		
		
	}
}


























